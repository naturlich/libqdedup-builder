#!/bin/bash

HOSTDIR=/home/$USER/working

BUILDER_1=docker.io/naturlich/x86_64-centos7

BUILDER_OPTS="
    --privileged \
    --net=host \
     \
    -e PNAME=$1-builder-$2 \
    -e "TZ=Asia/Taipei" \
    -u root \
    -w /root \
    -v $HOSTDIR:/root \
    -v /mnt/data/$USER:/root/extension \
    --name=$USER-$1-$2"

case $1 in
	cleanup)
		if [ "$EUID" -ne 0 ]; then
		    echo
		    echo "Please run as root or sudo user."
		    echo
		    exit
		fi
		docker rm -f $(docker ps -aq)
		docker rmi $(docker images -q)
		echo
		echo "Removed all containers & images"
		echo
		exit
		;;
	x86_64)
		docker pull naturlich/x86_64-centos7
		docker run  $BUILDER_OPTS -tid $BUILDER_1 bash
		;;
	*)
esac
